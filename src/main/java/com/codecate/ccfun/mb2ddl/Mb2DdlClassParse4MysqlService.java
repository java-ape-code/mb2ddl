package com.codecate.ccfun.mb2ddl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.codecate.ccfun.mb2ddl.annotation.Index;
import com.codecate.ccfun.mb2ddl.annotation.Unique;
import com.codecate.ccfun.mb2ddl.constants.Constants;
import com.codecate.ccfun.mb2ddl.dao.Mb2DdlCreateMysqlTablesMapper;
import com.codecate.ccfun.mb2ddl.model.Mb2DdlColumn;
import com.codecate.ccfun.mb2ddl.model.Mb2DdlTable;
import com.codecate.ccfun.mb2ddl.model.MySqlTypeAndLength;
import com.codecate.ccfun.mb2ddl.model.MysqlColumn;
import com.codecate.ccfun.mb2ddl.model.MysqlTable;
import com.codecate.ccfun.mb2ddl.model.TableConfig;
import com.codecate.ccfun.mb2ddl.utils.ColumnUtils;


@Service
public class Mb2DdlClassParse4MysqlService {
	
	@Autowired
	private Mb2DdlCreateMysqlTablesMapper createMysqlTablesMapper;
	@Autowired
	private Mb2DdlConfigurationUtil springContextUtil;

	private void checkTableNames(Set<Class> classes) {
		List<String> tableNames = new ArrayList<>();
		for (Class<?> clazz : classes) {
			String tableName = ColumnUtils.getTableName(clazz);
			if (tableNames.contains(tableName)){
				throw new RuntimeException(tableName + "表名出现重复，禁止创建！");
			}
			tableNames.add(tableName);
		}
	}
	
	public List<Mb2DdlTable> parseClazzToTables(Set<Class> classes) {
		checkTableNames(classes);
		List<Mb2DdlTable> result = new ArrayList<>();
		for (Class clazz : classes) {
			Mb2DdlTable tableInfo = new Mb2DdlTable();
			tableInfo.setTableName(ColumnUtils.getTableName(clazz));
			tableInfo.setTableComment(ColumnUtils.getTableComment(clazz));
			tableInfo.setTableCharset(ColumnUtils.getTableCharset(clazz));
			tableInfo.setTableEngine(ColumnUtils.getTableEngine(clazz));
			parseClassColumnsAndIdxAndUnique(clazz,tableInfo);
			
			MysqlTable table = createMysqlTablesMapper.findTableByTableName(tableInfo.getTableName());
			tableInfo.setDbMysqlTable(table);
			result.add(tableInfo);
			// 不存在时
			if (table == null) {
	//			Map<String, Object> map = new HashMap<String, Object>();
	//			if (!StringUtils.isEmpty(tableComment)){
	//				map.put(MysqlTable.TABLE_COMMENT_KEY, tableComment);
	//			}
	//			if (tableCharset != null && tableCharset != Constants.MySqlCharset.DEFAULT){
	//				map.put(MysqlTable.TABLE_COLLATION_KEY, tableCharset.toString().toLowerCase());
	//			}
	//			if (tableEngine != null && tableEngine != Constants.MySqlEngine.DEFAULT){
	//				map.put(MysqlTable.TABLE_ENGINE_KEY, tableEngine.toString());
	//			}
	//			baseTableMap.get(Constants.NEW_TABLE_MAP).put(tableName, new TableConfig(allFieldList, map));
	//			baseTableMap.get(Constants.ADDINDEX_TABLE_MAP).put(tableName, new TableConfig(getAddIndexList(null, allFieldList)));
	//			baseTableMap.get(Constants.ADDUNIQUE_TABLE_MAP).put(tableName, new TableConfig(getAddUniqueList(null, allFieldList)));
	//			return;
			}else{
	//			Map<String, Object> map = new HashMap<String, Object>();
	//			// 判断表注释是否要更新
	//			if (!StringUtils.isEmpty(tableComment) && !tableComment.equals(table.getTable_comment())){
	//				map.put(MysqlTable.TABLE_COMMENT_KEY, tableComment);
	//			}
	//			// 判断表字符集是否要更新
	//			if (tableCharset != null && tableCharset != Constants.MySqlCharset.DEFAULT && !tableCharset.toString().toLowerCase().equals(table.getTable_collation().replace(MysqlTable.TABLE_COLLATION_SUFFIX, ""))){
	//				map.put(MysqlTable.TABLE_COLLATION_KEY, tableCharset.toString().toLowerCase());
	//			}
	//			// 判断表引擎是否要更新
	//			if (tableEngine != null && tableEngine != Constants.MySqlEngine.DEFAULT && !tableEngine.toString().equals(table.getEngine())){
	//				map.put(MysqlTable.TABLE_ENGINE_KEY, tableEngine.toString());
	//			}
	//			baseTableMap.get(Constants.MODIFY_TABLE_PROPERTY_MAP).put(tableName, new TableConfig(map));
			}
	
			// 已存在时理论上做修改的操作，这里查出该表的结构
			List<MysqlColumn> tableColumnList = createMysqlTablesMapper.findTableEnsembleByTableName(tableInfo.getTableName());
			tableInfo.setDbTableColumnList(tableColumnList);
//			// 从sysColumns中取出我们需要比较的列的List
//			// 先取出name用来筛选出增加和删除的字段
//			List<String> columnNames = ClassTools.getPropertyValueList(tableColumnList, MysqlColumns.COLUMN_NAME_KEY);
//	
//			// 验证对比从model中解析的allFieldList与从数据库查出来的columnList
//			// 2. 找出增加的字段
//			List<Object> addFieldList = getAddFieldList(allFieldList, columnNames);
//	
//			// 3. 找出删除的字段
//			List<Object> removeFieldList = getRemoveFieldList(columnNames, allFieldList);
//	
//			// 4. 找出更新的字段
//			List<Object> modifyFieldList = getModifyFieldList(tableColumnList, allFieldList);
//	
//			// 5. 找出需要删除主键的字段
//			List<Object> dropKeyFieldList = getDropKeyFieldList(tableColumnList, allFieldList);
//	
			String uniPrefix = springContextUtil.getConfig(Constants.TABLE_UNIQUE_KEY);
			String idxPrefix = springContextUtil.getConfig(Constants.TABLE_INDEX_KEY);
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("tableName", tableInfo.getTableName());
			paramMap.put("uniquePrefix",uniPrefix);
			paramMap.put("indexPrefix",idxPrefix);
//			// 查询当前表中全部acteble创建的索引和唯一约束，也就是名字前缀是actable_和actable_的
			List<String> allIndexAndUniqueNames = createMysqlTablesMapper.findTableIndexByTableName(paramMap);
			tableInfo.setDbIndexNameList(allIndexAndUniqueNames);
//			// 6. 找出需要删除的索引和唯一约束
//			List<Object> dropIndexAndUniqueFieldList = getDropIndexAndUniqueList(allIndexAndUniqueNames, allFieldList);
//	
//			// 7. 找出需要新增的索引
//			List<Object> addIndexFieldList = getAddIndexList(allIndexAndUniqueNames, allFieldList);
//	
//			// 8. 找出需要新增的唯一约束
//			List<Object> addUniqueFieldList = getAddUniqueList(allIndexAndUniqueNames, allFieldList);
//	
//			if (addFieldList.size() != 0) {
//				baseTableMap.get(Constants.ADD_TABLE_MAP).put(tableName, new TableConfig(addFieldList));
//			}
//			if (removeFieldList.size() != 0) {
//				baseTableMap.get(Constants.REMOVE_TABLE_MAP).put(tableName, new TableConfig(removeFieldList));
//			}
//			if (modifyFieldList.size() != 0) {
//				baseTableMap.get(Constants.MODIFY_TABLE_MAP).put(tableName, new TableConfig(modifyFieldList));
//			}
//			if (dropKeyFieldList.size() != 0) {
//				baseTableMap.get(Constants.DROPKEY_TABLE_MAP).put(tableName, new TableConfig(dropKeyFieldList));
//			}
//			if (dropIndexAndUniqueFieldList.size() != 0) {
//				baseTableMap.get(Constants.DROPINDEXANDUNIQUE_TABLE_MAP).put(tableName, new TableConfig(dropIndexAndUniqueFieldList));
//			}
//			if (addIndexFieldList.size() != 0) {
//				baseTableMap.get(Constants.ADDINDEX_TABLE_MAP).put(tableName, new TableConfig(addIndexFieldList));
//			}
//			if (addUniqueFieldList.size() != 0) {
//				baseTableMap.get(Constants.ADDUNIQUE_TABLE_MAP).put(tableName, new TableConfig(addUniqueFieldList));
//			}
		}
		return result;
	}
	
	private void createOrModifyTableConstruct(Map<String, Map<String, TableConfig>> baseTableMap) {

//		// 1. 创建表
//		createTableByMap(baseTableMap.get(Constants.NEW_TABLE_MAP));
//
//		// add是追加模式不做删除和修改操作
//		if(!"add".equals(active)){
//			// 2. 删除要变更主键的表的原来的字段的主键
//			dropFieldsKeyByMap(baseTableMap.get(Constants.DROPKEY_TABLE_MAP));
//		}
//
//		// add是追加模式不做删除和修改操作
//		if(!"add".equals(active)) {
//			// 3. 删除索引和约束
//			dropIndexAndUniqueByMap(baseTableMap.get(Constants.DROPINDEXANDUNIQUE_TABLE_MAP));
//			// 4. 删除字段
//			removeFieldsByMap(baseTableMap.get(Constants.REMOVE_TABLE_MAP));
//			// 5. 修改表注释
//			modifyTableCommentByMap(baseTableMap.get(Constants.MODIFY_TABLE_PROPERTY_MAP));
//			// 6. 修改字段类型等
//			modifyFieldsByMap(baseTableMap.get(Constants.MODIFY_TABLE_MAP));
//		}
//
//		// 7. 添加新的字段
//		addFieldsByMap(baseTableMap.get(Constants.ADD_TABLE_MAP));
//
//		// 8. 创建索引
//		addIndexByMap(baseTableMap.get(Constants.ADDINDEX_TABLE_MAP));
//
//		// 9. 创建约束
//		addUniqueByMap(baseTableMap.get(Constants.ADDUNIQUE_TABLE_MAP));

	}
	
	
	
	
	
	
	public void parseClassColumnsAndIdxAndUnique(Class<?> clazz,Mb2DdlTable tableInfo) {
		String idxPrefix = springContextUtil.getConfig(Constants.TABLE_INDEX_KEY);
		String uniPrefix = springContextUtil.getConfig(Constants.TABLE_UNIQUE_KEY);
		List<Field> fieldList = new ArrayList<>();
		parseClassAllFields(clazz, fieldList);
		if (!CollectionUtils.isEmpty(fieldList)) {
			for (Field field : fieldList) {
				// 判断方法中是否有指定注解类型的注解
				if (ColumnUtils.columnNotExistValue(field,clazz)) {
					Mb2DdlColumn column = new Mb2DdlColumn();
					column.setFieldName(ColumnUtils.getColumnName(field,clazz));
					MySqlTypeAndLength mySqlTypeAndLength = ColumnUtils.getMySqlTypeAndLength(field,clazz);
					column.setFieldType(mySqlTypeAndLength.getType().toLowerCase());
					column.setFileTypeLength(mySqlTypeAndLength.getLengthCount());
					if (mySqlTypeAndLength.getLengthCount() == 1) {
						column.setFieldLength(mySqlTypeAndLength.getLength());
					} else if (mySqlTypeAndLength.getLengthCount() == 2) {
						column.setFieldLength(mySqlTypeAndLength.getLength());
						column.setFieldDecimalLength(mySqlTypeAndLength.getDecimalLength());
					}
					column.setFieldIsNull(ColumnUtils.isNull(field,clazz));
					column.setFieldIsKey(ColumnUtils.isKey(field,clazz));
					column.setFieldIsAutoIncrement(ColumnUtils.isAutoIncrement(field,clazz));
					column.setFieldDefaultValue(ColumnUtils.getDefaultValue(field,clazz));
					column.setFieldDefaultValueNative(ColumnUtils.getDefaultValueNative(field,clazz));
					column.setFieldComment(ColumnUtils.getComment(field,clazz));
//					 获取当前字段的@Index注解
					Index index = field.getAnnotation(Index.class);
					if (null != index) {
						String[] indexValue = index.columns();
						column.setFiledIndexName((index.value() == null || index.value().equals(""))
								? (idxPrefix + ((indexValue.length == 0) ? ColumnUtils.getColumnName(field,clazz) : stringArrFormat(indexValue)))
								: idxPrefix + index.value());
						column.setFiledIndexValue(
								indexValue.length == 0 ? Arrays.asList(ColumnUtils.getColumnName(field,clazz)) : Arrays.asList(indexValue));
					}
//					 获取当前字段的@Unique注解
					Unique unique = field.getAnnotation(Unique.class);
					if (null != unique) {
						String[] uniqueValue = unique.columns();
						column.setFiledUniqueName((unique.value() == null || unique.value().equals(""))
								? (uniPrefix
								+ ((uniqueValue.length == 0) ? ColumnUtils.getColumnName(field,clazz) : stringArrFormat(uniqueValue)))
								: uniPrefix + unique.value());
						column.setFiledUniqueValue(
								uniqueValue.length == 0 ? Arrays.asList(ColumnUtils.getColumnName(field,clazz)) : Arrays.asList(uniqueValue));
					}
					
					tableInfo.getTableColumns().add(column);
				}
			}
		}
	}
	
	private void parseClassAllFields(Class<?> clazz, List<Field> fieldList) {
		if (clazz.getSuperclass() != null) {
			Class clazzSup = clazz.getSuperclass();
			parseClassAllFields(clazzSup, fieldList);
		}
		Field[] clazzFields = clazz.getDeclaredFields();
		if (null!=clazzFields && clazzFields.length>0) {
			for (Field field : clazzFields) {
				boolean have = false;
				if (!CollectionUtils.isEmpty(fieldList)) {
					int i=0;
					for (Field haveField : fieldList) {
						//同名覆盖 父类的属性
						if (StringUtils.equals(haveField.getName(), field.getName())) {
							fieldList.set(i, field);
						}
						i++;
					}
				}
				if (!have) {
					fieldList.add(field);
				}
			}
		}		
	}
	
	private String stringArrFormat(String[] arr) {
		return String.valueOf(Arrays.toString(arr)).replaceAll(",", "_").replaceAll(" ", "").replace("[", "")
				.replace("]", "");
	}
	
}
