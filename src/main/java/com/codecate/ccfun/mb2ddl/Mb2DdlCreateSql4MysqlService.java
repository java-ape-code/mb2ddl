package com.codecate.ccfun.mb2ddl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.codecate.ccfun.mb2ddl.log.Mb2DdlLog;
import com.codecate.ccfun.mb2ddl.model.Mb2DdlColumn;
import com.codecate.ccfun.mb2ddl.model.Mb2DdlTable;

@Service
public class Mb2DdlCreateSql4MysqlService {
	
	
	public String makeCreateTableSql(Mb2DdlTable mb2DdlTable, List<Mb2DdlColumn> allKeys) {
		String commonSql = "";
		
		if (!CollectionUtils.isEmpty(mb2DdlTable.getTableColumns())) {
			int i=0;
			for (Mb2DdlColumn column : mb2DdlTable.getTableColumns()) {
				
				if (0==column.getFileTypeLength()) {
					commonSql += "`"+column.getFieldName() + "` " + column.getFieldType();
				}else if (1==column.getFileTypeLength()){
					commonSql += "`"+column.getFieldName() + "` " + column.getFieldType() + " ("+ column.getFieldLength() 
					          + ")";
				}else if (2==column.getFileTypeLength()){
					commonSql += "`"+column.getFieldName() + "` " + column.getFieldType() + " ("+ column.getFieldLength() 
			          +  "," + column.getFieldDecimalLength()  + ")";
				}
				if (column.isFieldIsNull()) {
					commonSql += " NULL";
				}else {
					commonSql += " NOT NULL";
				}
				if (column.isFieldIsAutoIncrement()) {
					commonSql += " AUTO_INCREMENT";
				}
				
				
				
				
				
				
				if (StringUtils.isNotBlank(column.getFieldComment())) {
					commonSql += " COMMENT " + "'" + column.getFieldComment()+ "'";
				}
				
				
				if (i<mb2DdlTable.getTableColumns().size()-1) {
					commonSql += ",";
				}
				
				
				i++;
				
			}
		}
		
		
		
		
		
//		<sql id="commonSql">
//		<if test="fields.fileTypeLength == 0">
//			`${fields.fieldName}` ${fields.fieldType}
//		</if>
//		<if test="fields.fileTypeLength == 1">
//			`${fields.fieldName}` ${fields.fieldType}(${fields.fieldLength})
//		</if>
//		<if test="fields.fileTypeLength == 2">
//			`${fields.fieldName}`
//			${fields.fieldType}(${fields.fieldLength},${fields.fieldDecimalLength})
//		</if>
//		<if test="fields.fieldIsNull">
//			NULL
//		</if>
//		<if test="!fields.fieldIsNull">
//			NOT NULL
//		</if>
//		<if test="fields.fieldIsAutoIncrement">
//			AUTO_INCREMENT
//		</if>
		
//		<!-- 不是自增长的才能有默认值 -->
//		<!-- 并且不为null时，因为null是默认的没必要写 -->
//		<if test="!fields.fieldIsAutoIncrement and !fields.fieldIsNull and fields.fieldDefaultValue != null">
//			<if test="fields.fieldType == 'bit' and !fields.fieldDefaultValueNative">
//				<if test="fields.fieldDefaultValue == 'true'.toString()">
//					DEFAULT 1
//				</if>
//				<if test="fields.fieldDefaultValue == 'false'.toString()">
//					DEFAULT 0
//				</if>
//				<if test="fields.fieldDefaultValue == '1'.toString()">
//					DEFAULT 1
//				</if>
//				<if test="fields.fieldDefaultValue == '0'.toString()">
//					DEFAULT 0
//				</if>
//			</if>
//			<if test="fields.fieldType == 'bit' and fields.fieldDefaultValueNative">
//				DEFAULT ${fields.fieldDefaultValue}
//			</if>
//			<if test="fields.fieldType != 'bit'">
//				<if test="fields.fieldDefaultValueNative">
//					DEFAULT ${fields.fieldDefaultValue}
//				</if>
//				<if test="!fields.fieldDefaultValueNative">
//					DEFAULT #{fields.fieldDefaultValue}
//				</if>
//			</if>
//		</if>
//		<!-- 不是自增长的才能有默认值 -->
//		<!-- 允许是空时，并且默认值不是NUll -->
//		<if test="!fields.fieldIsAutoIncrement and fields.fieldIsNull and fields.fieldDefaultValue != null">
//			<if test="fields.fieldType == 'bit' and !fields.fieldDefaultValueNative">
//				<if test="fields.fieldDefaultValue == 'true'.toString()">
//					DEFAULT 1
//				</if>
//				<if test="fields.fieldDefaultValue == 'false'.toString()">
//					DEFAULT 0
//				</if>
//				<if test="fields.fieldDefaultValue == '1'.toString()">
//					DEFAULT 1
//				</if>
//				<if test="fields.fieldDefaultValue == '0'.toString()">
//					DEFAULT 0
//				</if>
//			</if>
//			<if test="fields.fieldType == 'bit' and fields.fieldDefaultValueNative">
//				DEFAULT ${fields.fieldDefaultValue}
//			</if>
//			<if test="fields.fieldType != 'bit'">
//				<if test="fields.fieldDefaultValueNative">
//					DEFAULT ${fields.fieldDefaultValue}
//				</if>
//				<if test="!fields.fieldDefaultValueNative">
//					DEFAULT #{fields.fieldDefaultValue}
//				</if>
//			</if>
//		</if>
//		<if test="fields.fieldComment != ''">
//			COMMENT #{fields.fieldComment}
//		</if>
//	</sql>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		String sql = "create table `"+mb2DdlTable.getTableName() + "` (" + commonSql;
		if (!CollectionUtils.isEmpty(allKeys)) {
			sql += ",PRIMARY KEY (";
			int i = 0;
			for (Mb2DdlColumn mb2DdlColumn : allKeys) {
				sql += mb2DdlColumn.getFieldName();
				if (i<allKeys.size()-1) {
					sql += ",";
				}
			}
			sql+=")";
		}
		sql += ")";
		
		if (StringUtils.isNotBlank(mb2DdlTable.getTableComment())) {
			sql += " COMMENT = '" + mb2DdlTable.getTableComment() + "'";
		}
		if (null!=mb2DdlTable.getTableCharset()) {
			sql += " CHARSET =" + mb2DdlTable.getTableCharset();
		}
		if (null!=mb2DdlTable.getTableEngine()) {
			sql += " ENGINE =" + mb2DdlTable.getTableEngine();
		}
		sql += ";";
		
		
		
//		<foreach collection="tableMap" index="key" item="value">
//		create table `${key}`(
//		<foreach collection="value.list" item="fields" separator=",">
//			<include refid="commonSql"></include>
//		</foreach>
//			,PRIMARY KEY (
//			<foreach collection="tableKeys" index="index" item="columnName">
//					<if test="tableKeys.size == 1">
//						${columnName}
//					</if>
//					<if test="tableKeys.size > 1">
//						${columnName}
//						<if test="tableKeys.size != index + 1">
//						,
//						</if>
//					</if>
//			</foreach>
//			)
//		)
//		<foreach collection="value.map" index="ckey" item="cvalue">
//			<if test="ckey == 'table_comment'">
//				COMMENT = #{cvalue}
//			</if>
//			<if test="ckey == 'table_collation'">
//				CHARSET = #{cvalue}
//			</if>
//			<if test="ckey == 'engine'">
//				ENGINE = #{cvalue}
//			</if>
//		</foreach>
//		;
//	</foreach>		
		
		
		


		
		
		
		
		
		
		
		
		
		
		Mb2DdlLog.logErr(sql);

	
		
		
		return sql;
	}
	
	public List<String> makeCreateIndexSql(Mb2DdlTable mb2DdlTable, List<Mb2DdlColumn> allIndexs) {
		List<String> sqlList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(allIndexs)) {
			for (Mb2DdlColumn column : allIndexs) {
				String sql = "CREATE INDEX "+column.getFiledIndexName();
				sql += " ON " + mb2DdlTable.getTableName() + "(" + column.getFiledIndexValue() + ");";
				sqlList.add(sql);
				Mb2DdlLog.logErr(sql);
			}
		}
//		<foreach collection="tableMap" index="tableName" item="fields" separator=";">
//		CREATE INDEX ${fields.filedIndexName}
//		ON `${tableName}` (
//		<foreach collection="fields.filedIndexValue" index="index" item="columnName">
//			<if test="fields.filedIndexValue.size == 1">
//				${columnName}
//			</if>
//			<if test="fields.filedIndexValue.size > 1">
//				${columnName}
//				<if test="fields.filedIndexValue.size != index + 1">
//				,
//				</if>
//			</if>
//		</foreach>
//		)
//</foreach>
		
		
		return sqlList;
	}
	
	
	public List<String> makeCreateUniqueSql(Mb2DdlTable mb2DdlTable, List<Mb2DdlColumn> allUniques) {
		List<String> sqlList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(allUniques)) {
			for (Mb2DdlColumn column : allUniques) {
				String sql = "CREATE UNIQUE INDEX "+column.getFiledUniqueName();
				sql += " ON " + mb2DdlTable.getTableName() + "(" + column.getFiledUniqueValue() + ");";
				sqlList.add(sql);
				Mb2DdlLog.logErr(sql);
			}
		}
//		foreach collection="tableMap" index="tableName" item="fields" separator=";">
//		CREATE UNIQUE INDEX ${fields.filedUniqueName}
//		ON `${tableName}` (
//		<foreach collection="fields.filedUniqueValue" index="index" item="columnName">
//			<if test="fields.filedUniqueValue.size == 1">
//				${columnName}
//			</if>
//			<if test="fields.filedUniqueValue.size > 1">
//				${columnName}
//				<if test="fields.filedUniqueValue.size != index + 1">
//				,
//				</if>
//			</if>
//		</foreach>
//		)
//</foreach>
		
		
		return sqlList;
	}
	

}
