package com.codecate.ccfun.mb2ddl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.codecate.ccfun.mb2ddl.constants.Constants;
import com.codecate.ccfun.mb2ddl.dao.Mb2DdlCreateMysqlTablesMapper;
import com.codecate.ccfun.mb2ddl.log.Mb2DdlLog;
import com.codecate.ccfun.mb2ddl.model.Mb2DdlColumn;
import com.codecate.ccfun.mb2ddl.model.Mb2DdlTable;
import com.codecate.ccfun.mb2ddl.model.MysqlColumn;
import com.codecate.ccfun.mb2ddl.model.MysqlTable;
import com.codecate.ccfun.mb2ddl.model.TableConfig;

@Service
public class Mb2DdlModify2DbService {

	@Autowired
	private Mb2DdlCreateMysqlTablesMapper createMysqlTablesMapper;

	// 1. 创建表
	public void createTableByMap(Mb2DdlTable mb2DdlTable,List<Mb2DdlColumn> currKeyFieldList) {
		Map<String, Object> tableMap = new HashMap<String, Object>();
		if (!StringUtils.isEmpty(mb2DdlTable.getTableComment())) {
			tableMap.put(MysqlTable.TABLE_COMMENT_KEY, mb2DdlTable.getTableComment());
		}
		if (mb2DdlTable.getTableCharset() != null && mb2DdlTable.getTableCharset() != Constants.MySqlCharset.DEFAULT) {
			tableMap.put(MysqlTable.TABLE_COLLATION_KEY, mb2DdlTable.getTableCharset().toString().toLowerCase());
		}
		if (mb2DdlTable.getTableEngine() != null && mb2DdlTable.getTableEngine() != Constants.MySqlEngine.DEFAULT) {
			tableMap.put(MysqlTable.TABLE_ENGINE_KEY, mb2DdlTable.getTableEngine().toString());
		}

		Map<String, TableConfig> dbmap = new HashMap<String, TableConfig>();
		dbmap.put(mb2DdlTable.getTableName(), new TableConfig((mb2DdlTable.getTableColumns()), tableMap));
		
		List<String> tableKeys = new ArrayList<>();
		if (!CollectionUtils.isEmpty(currKeyFieldList)) {
			for (Mb2DdlColumn column : currKeyFieldList) {
				tableKeys.add(column.getFieldName());
			}
		}

		Mb2DdlLog.logErr("开始创建表：" + mb2DdlTable.getTableName());
		try {
			createMysqlTablesMapper.createTable(dbmap,tableKeys);
		} catch (Exception e) {
			e.printStackTrace();
			Mb2DdlLog.logErr(e);
		}
		Mb2DdlLog.logErr("完成创建表：" + mb2DdlTable.getTableName());
	}

	// 2. 删除要变更主键的表的原来的字段的主键
	public void dropFieldsKeyByMap(Mb2DdlTable mb2DdlTable, List<Mb2DdlColumn> dropKeyFieldList) {
		if (!CollectionUtils.isEmpty(dropKeyFieldList)) {
			for (Mb2DdlColumn mb2DdlColumn : dropKeyFieldList) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put(mb2DdlTable.getTableName(), mb2DdlColumn);
				Mb2DdlLog.logErr("开始为表" + mb2DdlTable.getTableName() + "删除主键" + mb2DdlColumn.getFieldName());
				try {
					createMysqlTablesMapper.dropKeyTableField(map);
				} catch (Exception e) {
					e.printStackTrace();
					Mb2DdlLog.logErr(e);
				}
				Mb2DdlLog.logErr("完成为表" + mb2DdlTable.getTableName() + "删除主键" + mb2DdlColumn.getFieldName());
			}
		}
	}
	
	public void changeFieldsKeyByMap(Mb2DdlTable mb2DdlTable, List<Mb2DdlColumn> currKeyFieldList) {
		if (!CollectionUtils.isEmpty(currKeyFieldList)) {
			Map<String, Object> map = new HashMap<String, Object>();
			
			List<String> names = new ArrayList<>();
			for (Mb2DdlColumn column : currKeyFieldList) {
				names.add(column.getFieldName());
			}
			map.put(mb2DdlTable.getTableName(), names);
			Mb2DdlLog.logErr("开始为表" + mb2DdlTable.getTableName() + "修改主键" + names);
			try {
				createMysqlTablesMapper.changeKeyTableField(map);
			} catch (Exception e) {
				e.printStackTrace();
				Mb2DdlLog.logErr(e);
			}
			Mb2DdlLog.logErr("完成为表" + mb2DdlTable.getTableName() + "修改主键" + names);
		}
	}
	

	// 3. 删除索引和约束
	public void dropIndexAndUniqueByMap(Mb2DdlTable mb2DdlTable, List<String> dropIndexAndUniqueFieldList) {
		if (!CollectionUtils.isEmpty(dropIndexAndUniqueFieldList)) {
			for (String name : dropIndexAndUniqueFieldList) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put(mb2DdlTable.getTableName(), name);
				Mb2DdlLog.logErr("开始删除表" + mb2DdlTable.getTableName() + "中的索引" + name);
				try {
					createMysqlTablesMapper.dropTabelIndex(map);
				} catch (Exception e) {
					e.printStackTrace();
					Mb2DdlLog.logErr(e);
				}
				Mb2DdlLog.logErr("完成删除表" + mb2DdlTable.getTableName() + "中的索引" + name);
			}
		}
	}

	// 4. 删除字段
	public void removeFieldsByMap(Mb2DdlTable mb2DdlTable, List<MysqlColumn> removeFieldList) {
		if (!CollectionUtils.isEmpty(removeFieldList)) {
			for (MysqlColumn column : removeFieldList) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put(mb2DdlTable.getTableName(), column.getColumn_name());
				Mb2DdlLog.logErr("开始删除表" + mb2DdlTable.getTableName() + "中的字段" + column.getColumn_name());
				try {
					createMysqlTablesMapper.removeTableField(map);
				} catch (Exception e) {
					e.printStackTrace();
					Mb2DdlLog.logErr(e);
				}
				Mb2DdlLog.logErr("完成删除表" + mb2DdlTable.getTableName() + "中的字段" + column.getColumn_name());
			}
		}
	}

	// 5. 修改表注释
	public void modifyTableCommentByMap(Mb2DdlTable mb2DdlTable) {
		if (null == mb2DdlTable.getDbMysqlTable()) {
			return;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		// 判断表注释是否要更新
		if (!StringUtils.isEmpty(mb2DdlTable.getTableComment())
				&& !mb2DdlTable.getTableComment().equals(mb2DdlTable.getDbMysqlTable().getTable_comment())) {
			map.put(MysqlTable.TABLE_COMMENT_KEY, mb2DdlTable.getTableComment());
		}
		// 判断表字符集是否要更新
		if (mb2DdlTable.getTableCharset() != null && mb2DdlTable.getTableCharset() != Constants.MySqlCharset.DEFAULT
				&& !mb2DdlTable.getTableCharset().toString().toLowerCase().equals(mb2DdlTable.getDbMysqlTable()
						.getTable_collation().replace(MysqlTable.TABLE_COLLATION_SUFFIX, ""))) {
			map.put(MysqlTable.TABLE_COLLATION_KEY, mb2DdlTable.getTableCharset().toString().toLowerCase());
		}
		// 判断表引擎是否要更新
		if (mb2DdlTable.getTableEngine() != null && mb2DdlTable.getTableEngine() != Constants.MySqlEngine.DEFAULT
				&& !mb2DdlTable.getTableEngine().toString().equals(mb2DdlTable.getDbMysqlTable().getEngine())) {
			map.put(MysqlTable.TABLE_ENGINE_KEY, mb2DdlTable.getTableEngine().toString());
		}
		for (String property : map.keySet()) {
			Map<String, TableConfig> dbMap = new HashMap<String, TableConfig>();
			Map<String, Object> tcMap = new HashMap<String, Object>();
			Object value = map.get(property);
			tcMap.put(property, value);
			dbMap.put(mb2DdlTable.getTableName(), new TableConfig(tcMap));
			Mb2DdlLog.logErr("开始更新表" + mb2DdlTable.getTableName() + "的" + property + "为" + value);
			try {
				createMysqlTablesMapper.modifyTableProperty(dbMap);
			} catch (Exception e) {
				e.printStackTrace();
				Mb2DdlLog.logErr(e);
			}
			Mb2DdlLog.logErr("完成更新表" + mb2DdlTable.getTableName() + "的" + property + "为" + value);
		}
	}

	// 6. 修改字段类型等
	public void modifyFieldsByMap(Mb2DdlTable mb2DdlTable, List<Mb2DdlColumn> modifyFieldList) {
		if (!CollectionUtils.isEmpty(modifyFieldList)) {
			for (Mb2DdlColumn column : modifyFieldList) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put(mb2DdlTable.getTableName(), column);
				Mb2DdlLog.logErr("开始修改表" + mb2DdlTable.getTableName() + "中的字段" + column.getFieldName());
				try {
					createMysqlTablesMapper.modifyTableField(map);
				} catch (Exception e) {
					e.printStackTrace();
					Mb2DdlLog.logErr(e);
				}
				Mb2DdlLog.logErr("完成修改表" + mb2DdlTable.getTableName() + "中的字段" + column.getFieldName());
			}
		}
	}

	// 7. 添加新的字段
	public void addFieldsByMap(Mb2DdlTable mb2DdlTable, List<Mb2DdlColumn> addFieldList) {
		if (!CollectionUtils.isEmpty(addFieldList)) {
			for (Mb2DdlColumn column : addFieldList) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put(mb2DdlTable.getTableName(), column);
				Mb2DdlLog.logErr("开始为表" + mb2DdlTable.getTableName() + "增加字段" + column.getFieldName());
				try {
					createMysqlTablesMapper.addTableField(map);
				} catch (Exception e) {
					e.printStackTrace();
					Mb2DdlLog.logErr(e);
				}
				Mb2DdlLog.logErr("完成为表" + mb2DdlTable.getTableName() + "增加字段" + column.getFieldName());
			}
		}
	}
	
	// 8. 创建索引
	public void addIndexByMap(Mb2DdlTable mb2DdlTable, List<Mb2DdlColumn> addIndexFieldList) {
		if (!CollectionUtils.isEmpty(addIndexFieldList)) {
			for (Mb2DdlColumn column : addIndexFieldList) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put(mb2DdlTable.getTableName(), column);
				Mb2DdlLog.logErr("开始创建表" + mb2DdlTable.getTableName() + "中的索引" + column.getFiledIndexName());
				try {
					createMysqlTablesMapper.addTableIndex(map);
				} catch (Exception e) {
					e.printStackTrace();
					Mb2DdlLog.logErr(e);
				}
				Mb2DdlLog.logErr("开始创建表" + mb2DdlTable.getTableName() + "中的索引" + column.getFiledIndexName());
			}
		}
	}

	// 9. 创建约束
	public void addUniqueByMap(Mb2DdlTable mb2DdlTable, List<Mb2DdlColumn> addUniqueFieldList) {
		if (!CollectionUtils.isEmpty(addUniqueFieldList)) {
			for (Mb2DdlColumn column : addUniqueFieldList) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put(mb2DdlTable.getTableName(), column);
				Mb2DdlLog.logErr("开始创建表" + mb2DdlTable.getTableName() + "中的唯一约束" + column.getFiledUniqueName());
				try {
					createMysqlTablesMapper.addTableUnique(map);
				} catch (Exception e) {
					e.printStackTrace();
					Mb2DdlLog.logErr(e);
				}
				Mb2DdlLog.logErr("完成创建表" + mb2DdlTable.getTableName() + "中的唯一约束" + column.getFiledUniqueName());
			}
		}
	}

}
