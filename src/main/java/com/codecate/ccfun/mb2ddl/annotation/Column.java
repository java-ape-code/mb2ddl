package com.codecate.ccfun.mb2ddl.annotation;

import com.codecate.ccfun.mb2ddl.constants.Constants;
import com.codecate.ccfun.mb2ddl.utils.ColumnUtils;

import java.lang.annotation.*;

/**
 * 建表的必备注解
 *
 */
// 该注解用于方法声明
@Target(ElementType.FIELD)
// VM将在运行期也保留注释，因此可以通过反射机制读取注解的信息
@Retention(RetentionPolicy.RUNTIME)
// 将此注解包含在javadoc中
@Documented
public @interface Column{

	/**
	 * 字段名
	 * @return 字段名：不填默认使用属性名作为表字段名
	 */
	String value() default "";

	/**
	 * 字段名
	 * @return 字段名：不填默认使用属性名作为表字段名
	 */
	String name() default "";

	/**
	 * 字段类型：不填默认使用属性的数据类型进行转换，转换失败的字段不会添加
	 * @return 字段类型
	 */
	Constants.MySqlType type() default Constants.MySqlType.DEFAULT;

	/**
	 * 字段长度，默认是255
	 * @return 默认字段长度，默认是255
	 */
	int length() default 255;

	/**
	 * 小数点长度，默认是0
	 * @return 小数点长度，默认是0
	 */
	int decimalLength() default 0;

	/**
	 * 是否为可以为null，true是可以，false是不可以，默认为true
	 * @return 是否为可以为null，true是可以，false是不可以，默认为true
	 */
	boolean isNull() default true;

	/**
	 * 是否是主键，默认false
	 * 1.3.0版本支持，类同javax.persistence.Id
	 * @return 是否是主键，默认false
	 */
	boolean isKey() default false;

	/**
	 * 是否自动递增，默认false
	 *
	 * @return 是否自动递增，默认false 只有主键才能使用
	 */
	boolean isAutoIncrement() default false;

	/**
	 * 默认值，默认为null
	 * @return 默认值
	 */
	String defaultValue() default ColumnUtils.DEFAULTVALUE;

	/**
	 * 数据表字段备注
	 * @return 默认值，默认为空
	 */
	String comment() default "";
	
	/**
     * 是否为数据库表字段
     * <p>
     * 默认 true 存在，false 不存在
     */
	boolean exist() default true;

}
