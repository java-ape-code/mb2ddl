package com.codecate.ccfun.mb2ddl.annotation.impl;

import com.codecate.ccfun.mb2ddl.annotation.*;
import com.codecate.ccfun.mb2ddl.constants.Constants;
import com.codecate.ccfun.mb2ddl.utils.ColumnUtils;

import java.lang.annotation.Annotation;

public class ColumnImpl implements Column {
    /**
     * 字段名
     * @return 字段名：不填默认使用属性名作为表字段名
     */
    @Override
    public String value() {
        return "";
    }

    /**
     * 字段名
     * @return 字段名：不填默认使用属性名作为表字段名
     */
    @Override
    public String name() {
        return "";
    }

    /**
     * 字段类型：不填默认使用属性的数据类型进行转换，转换失败的字段不会添加
     * @return 字段类型
     */
    @Override
    public Constants.MySqlType type() {
        return Constants.MySqlType.DEFAULT;
    }

    /**
     * 字段长度，默认是255
     * @return 默认字段长度，默认是255
     */
    @Override
    public int length() {
        return 255;
    }

    /**
     * 小数点长度，默认是0
     * @return 小数点长度，默认是0
     */
    @Override
    public int decimalLength() {
        return 0;
    }

    /**
     * 是否为可以为null，true是可以，false是不可以，默认为true
     * @return 是否为可以为null，true是可以，false是不可以，默认为true
     */
    @Override
    public boolean isNull() {
        return true;
    }

    /**
     * 是否是主键，默认false
     * @return 是否是主键，默认false
     */
    @Override
    public boolean isKey() {
        return false;
    }

    /**
     * 是否自动递增，默认false
     * @return 是否自动递增，默认false 只有主键才能使用
     */
    @Override
    public boolean isAutoIncrement() {
        return false;
    }

    /**
     * 默认值，默认为null
     * @return 默认值
     */
    @Override
    public String defaultValue() {
        return ColumnUtils.DEFAULTVALUE;
    }

//    /**
//     * 开启默认值原生模式
//     * 原生模式介绍：默认是false表示非原生，此时value只支持字符串形式，会将value值以字符串的形式设置到字段的默认值，例如value="aa" 即sql为 DEFAULT "aa"
//     * 如果设置isNative=true，此时如果value="current_timestamp"，即sql为 DEFAULT current_timestamp
//     *
//     * @return
//     */
//    @Override
//    public boolean isNativeDefValue() {
//        return false;
//    }

    /**
     * 数据表字段备注
     * @return 默认值，默认为空
     */
    @Override
    public String comment() {
        return "";
    }

    /**
     * Returns the annotation type of this annotation.
     *
     * @return the annotation type of this annotation
     */
    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }

	@Override
	public boolean exist() {
		return true;
	}
}
