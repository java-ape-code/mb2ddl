package com.codecate.ccfun.mb2ddl.annotation;

import java.lang.annotation.*;

import com.codecate.ccfun.mb2ddl.constants.Constants;


/**
 * 字段的类型
 *
 */
// 该注解用于方法声明
@Target(ElementType.FIELD)
// VM将在运行期也保留注释，因此可以通过反射机制读取注解的信息
@Retention(RetentionPolicy.RUNTIME)
// 将此注解包含在javadoc中
@Documented
public @interface ColumnType {

	/**
	 * 字段的类型
	 * @return 字段的类型
	 */
	Constants.MySqlType value() default Constants.MySqlType.DEFAULT;

	/**
	 * 字段长度，默认是255
	 * @return 字段长度，默认是255
	 */
	int length() default 255;

	/**
	 * 小数点长度，默认是0
	 * @return 小数点长度，默认是0
	 */
	int decimalLength() default 0;
}
