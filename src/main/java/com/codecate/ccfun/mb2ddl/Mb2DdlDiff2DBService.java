package com.codecate.ccfun.mb2ddl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.codecate.ccfun.mb2ddl.constants.Constants;
import com.codecate.ccfun.mb2ddl.model.Mb2DdlColumn;
import com.codecate.ccfun.mb2ddl.model.Mb2DdlTable;
import com.codecate.ccfun.mb2ddl.model.MysqlColumn;
import com.codecate.ccfun.mb2ddl.utils.ClassTools;
import com.codecate.ccfun.mb2ddl.utils.ColumnUtils;

public class Mb2DdlDiff2DBService {

	public List<Mb2DdlColumn> getAddFieldList(Mb2DdlTable mb2DdlTable) {
		List<String> dbColumns = new ArrayList<>();
		List<Mb2DdlColumn> addFieldList = new ArrayList<>();
		if (null == mb2DdlTable.getDbMysqlTable()) {
			return addFieldList;
		}
		for (MysqlColumn column : mb2DdlTable.getDbTableColumnList()) {
			dbColumns.add(column.getColumn_name());
		}
		List<String> toLowerCaseColumnNames = ClassTools.toLowerCase(dbColumns);
		for (Mb2DdlColumn column : mb2DdlTable.getTableColumns()) {
			// 循环新的model中的字段，判断是否在数据库中已经存在
			if (!toLowerCaseColumnNames.contains(column.getFieldName().toLowerCase())) {
				// 不存在，表示要在数据库中增加该字段
				addFieldList.add(column);
			}
		}
		return addFieldList;
	}

	public List<MysqlColumn> getRemoveFieldList(Mb2DdlTable mb2DdlTable) {
		List<MysqlColumn> removeFieldList = new ArrayList<>();
		for (MysqlColumn dbColumn : mb2DdlTable.getDbTableColumnList()) {
			boolean have = false;
			for (Mb2DdlColumn column : mb2DdlTable.getTableColumns()) {
				if (StringUtils.equalsIgnoreCase(dbColumn.getColumn_name(), column.getFieldName())) {
					have = true;
				}
			}
			if (!have) {
				removeFieldList.add(dbColumn);
			}
		}
		return removeFieldList;
	}

	public List<Mb2DdlColumn> getModifyFieldList(Mb2DdlTable mb2DdlTable) {
		Map<String, Mb2DdlColumn> fieldMap = getAllFieldMap(mb2DdlTable.getTableColumns());
		List<Mb2DdlColumn> modifyFieldList = new ArrayList<>();
		for (MysqlColumn sysColumn : mb2DdlTable.getDbTableColumnList()) {
			// 数据库中有该字段时，验证是否有更新
			Mb2DdlColumn createTableParam = fieldMap.get(sysColumn.getColumn_name().toLowerCase());
			if (createTableParam != null) {
				// 该复制操作时为了解决multiple primary key defined的同时又不会drop primary key
				Mb2DdlColumn modifyTableParam = createTableParam.clone();
				// 1.验证主键
				// 原本不是主键，现在变成了主键，那么要去做更新
				if (!"PRI".equals(sysColumn.getColumn_key()) && createTableParam.isFieldIsKey()) {
					modifyFieldList.add(modifyTableParam);
					continue;
				}
				// 原本是主键，现在依然主键，坚决不能在alter语句后加primary key，否则会报multiple primary
				// key defined
				if ("PRI".equals(sysColumn.getColumn_key()) && createTableParam.isFieldIsKey()) {
					modifyTableParam.setFieldIsKey(false);
				}
				// 2.验证类型
				if (!sysColumn.getData_type().toLowerCase().equals(createTableParam.getFieldType().toLowerCase())) {
					modifyFieldList.add(modifyTableParam);
					continue;
				}
				// 3.验证长度个小数点位数
				String typeAndLength = createTableParam.getFieldType().toLowerCase();
				if (createTableParam.getFileTypeLength() == 1) {
					// 拼接出类型加长度，比如varchar(1)
					typeAndLength = typeAndLength + "(" + createTableParam.getFieldLength() + ")";
				} else if (createTableParam.getFileTypeLength() == 2) {
					// 拼接出类型加长度，比如varchar(1)
					typeAndLength = typeAndLength + "(" + createTableParam.getFieldLength() + ","
							+ createTableParam.getFieldDecimalLength() + ")";
				}

				// 判断类型+长度是否相同
				if (!sysColumn.getColumn_type().toLowerCase().equals(typeAndLength)) {
					modifyFieldList.add(modifyTableParam);
					continue;
				}
				// 5.验证自增
				if ("auto_increment".equals(sysColumn.getExtra()) && !createTableParam.isFieldIsAutoIncrement()) {
					modifyFieldList.add(modifyTableParam);
					continue;
				}
				if (!"auto_increment".equals(sysColumn.getExtra()) && createTableParam.isFieldIsAutoIncrement()) {
					modifyFieldList.add(modifyTableParam);
					continue;
				}
				// 6.验证默认值
				if (sysColumn.getColumn_default() == null || sysColumn.getColumn_default().equals("")) {
					// 数据库默认值是null，model中注解设置的默认值不为NULL时，那么需要更新该字段
					if (createTableParam.getFieldDefaultValue() != null
							&& !ColumnUtils.DEFAULTVALUE.equals(createTableParam.getFieldDefaultValue())) {
						modifyFieldList.add(modifyTableParam);
						continue;
					}
				} else if (!sysColumn.getColumn_default().equals(createTableParam.getFieldDefaultValue())) {
					if (Constants.MySqlType.BIT.toString().toLowerCase().equals(createTableParam.getFieldType())
							&& !createTableParam.isFieldDefaultValueNative()) {
						if (("true".equals(createTableParam.getFieldDefaultValue())
								|| "1".equals(createTableParam.getFieldDefaultValue()))
								&& !"b'1'".equals(sysColumn.getColumn_default())) {
							// 两者不相等时，需要更新该字段
							modifyFieldList.add(modifyTableParam);
							continue;
						}
						if (("false".equals(createTableParam.getFieldDefaultValue())
								|| "0".equals(createTableParam.getFieldDefaultValue()))
								&& !"b'0'".equals(sysColumn.getColumn_default())) {
							// 两者不相等时，需要更新该字段
							modifyFieldList.add(modifyTableParam);
							continue;
						}
					} else {
						// 两者不相等时，需要更新该字段
						modifyFieldList.add(modifyTableParam);
						continue;
					}
				}
				// 7.验证是否可以为null(主键不参与是否为null的更新)
				if (sysColumn.getIs_nullable().equals("NO") && !createTableParam.isFieldIsKey()) {
					if (createTableParam.isFieldIsNull()) {
						// 一个是可以一个是不可用，所以需要更新该字段
						modifyFieldList.add(modifyTableParam);
						continue;
					}
				} else if (sysColumn.getIs_nullable().equals("YES") && !createTableParam.isFieldIsKey()) {
					if (!createTableParam.isFieldIsNull()) {
						// 一个是可以一个是不可用，所以需要更新该字段
						modifyFieldList.add(modifyTableParam);
						continue;
					}
				}
				// 8.验证注释
				if (!sysColumn.getColumn_comment().equals(createTableParam.getFieldComment())) {
					modifyFieldList.add(modifyTableParam);
				}
			}
		}
		return modifyFieldList;
	}

	public List<Mb2DdlColumn> getDropKeyFieldList(Mb2DdlTable mb2DdlTable) {
		Map<String, Mb2DdlColumn> fieldMap = getAllFieldMap(mb2DdlTable.getTableColumns());
		List<Mb2DdlColumn> dropKeyFieldList = new ArrayList<>();
		for (MysqlColumn sysColumn : mb2DdlTable.getDbTableColumnList()) {
			// 数据库中有该字段时
			Mb2DdlColumn createTableParam = fieldMap.get(sysColumn.getColumn_name().toLowerCase());
			if (createTableParam != null) {
				// 原本是主键，现在不是了，那么要去做删除主键的操作
				if ("PRI".equals(sysColumn.getColumn_key()) && !createTableParam.isFieldIsKey()) {
					dropKeyFieldList.add(createTableParam);
				}
			}
		}
		return dropKeyFieldList;
	}

	public List<Mb2DdlColumn> getCurrKeyFieldList(Mb2DdlTable mb2DdlTable) {
		List<Mb2DdlColumn> currKeyFieldList = new ArrayList<>();
		for (Mb2DdlColumn column : mb2DdlTable.getTableColumns()) {
			if (column.isFieldIsKey()) {
				currKeyFieldList.add(column);
			}
		}
		if (CollectionUtils.isEmpty(currKeyFieldList)) {
			return currKeyFieldList;
		}
		if (null == mb2DdlTable.getDbMysqlTable() || mb2DdlTable.getDbTableColumnList().size() == 0) {
			return currKeyFieldList;
		}
		// 验证主键字段在数据库中都存在
		boolean allHave = true;
		for (Mb2DdlColumn mb2DdlColumn : currKeyFieldList) {
			boolean have = false;
			for (MysqlColumn sysColumn : mb2DdlTable.getDbTableColumnList()) {
				if ("PRI".equals(sysColumn.getColumn_key())
						&& StringUtils.equals(sysColumn.getColumn_name(), mb2DdlColumn.getFieldName())) {
					have = true;
					continue;
				}
			}
			if (!have) {
				allHave = false;
			}
		}
		if (allHave) {
			//和数据库一致
			return null;
		}
		return currKeyFieldList;
	}
	
	
	public List<Mb2DdlColumn> getAllKeyFieldList(Mb2DdlTable mb2DdlTable) {
		List<Mb2DdlColumn> currKeyFieldList = new ArrayList<>();
		for (Mb2DdlColumn column : mb2DdlTable.getTableColumns()) {
			if (column.isFieldIsKey()) {
				currKeyFieldList.add(column);
			}
		}
		return currKeyFieldList;
	}

	private Map<String, Mb2DdlColumn> getAllFieldMap(List<Mb2DdlColumn> allFieldList) {
		// 将fieldList转成Map类型，字段名作为主键
		Map<String, Mb2DdlColumn> fieldMap = new HashMap<String, Mb2DdlColumn>();
		for (Mb2DdlColumn obj : allFieldList) {
			fieldMap.put(obj.getFieldName().toLowerCase(), obj);
		}
		return fieldMap;
	}

	public List<String> getDropIndexAndUniqueList(Mb2DdlTable mb2DdlTable) {
		List<String> allIndexAndUniqueNames = mb2DdlTable.getDbIndexNameList();
		List<String> dropIndexAndUniqueFieldList = new ArrayList<>();
		if (null == allIndexAndUniqueNames || allIndexAndUniqueNames.size() == 0) {
			return dropIndexAndUniqueFieldList;
		}
		List<String> currentModelIndexAndUnique = new ArrayList<String>();
		for (Mb2DdlColumn createTableParam : mb2DdlTable.getTableColumns()) {
			if (null != createTableParam.getFiledIndexName()) {
				currentModelIndexAndUnique.add(createTableParam.getFiledIndexName());
			}
			if (null != createTableParam.getFiledUniqueName()) {
				currentModelIndexAndUnique.add(createTableParam.getFiledUniqueName());
			}
		}
		for (String string : allIndexAndUniqueNames) {
			if (!currentModelIndexAndUnique.contains(string)) {
				dropIndexAndUniqueFieldList.add(string);
			}
		}
		return dropIndexAndUniqueFieldList;
	}

	public List<Mb2DdlColumn> getAddIndexList(Mb2DdlTable mb2DdlTable) {
		List<String> allIndexAndUniqueNames = mb2DdlTable.getDbIndexNameList();
		List<Mb2DdlColumn> addIndexFieldList = new ArrayList<>();
		if (null == allIndexAndUniqueNames) {
			allIndexAndUniqueNames = new ArrayList<>();
		}
		for (Mb2DdlColumn createTableParam : mb2DdlTable.getTableColumns()) {
			if (null != createTableParam.getFiledIndexName()
					&& !allIndexAndUniqueNames.contains(createTableParam.getFiledIndexName())) {
				addIndexFieldList.add(createTableParam);
			}
		}
		return addIndexFieldList;
	}
	
	
	
	public List<Mb2DdlColumn> getAllIndexList(Mb2DdlTable mb2DdlTable) {
		List<Mb2DdlColumn> addIndexFieldList = new ArrayList<>();
		for (Mb2DdlColumn createTableParam : mb2DdlTable.getTableColumns()) {
			if (null != createTableParam.getFiledIndexName()) {
				addIndexFieldList.add(createTableParam);
			}
		}
		return addIndexFieldList;
	}
	

	public List<Mb2DdlColumn> getAddUniqueList(Mb2DdlTable mb2DdlTable) {
		List<Mb2DdlColumn> addUniqueFieldList = new ArrayList<>();
		List<String> allIndexAndUniqueNames = mb2DdlTable.getDbIndexNameList();
		if (null == allIndexAndUniqueNames) {
			allIndexAndUniqueNames = new ArrayList<>();
		}
		for (Mb2DdlColumn createTableParam : mb2DdlTable.getTableColumns()) {
			if (null != createTableParam.getFiledUniqueName()
					&& !allIndexAndUniqueNames.contains(createTableParam.getFiledUniqueName())) {
				addUniqueFieldList.add(createTableParam);
			}
		}
		return addUniqueFieldList;
	}
	
	public List<Mb2DdlColumn> getAllUniqueList(Mb2DdlTable mb2DdlTable) {
		List<Mb2DdlColumn> addUniqueFieldList = new ArrayList<>();
		for (Mb2DdlColumn createTableParam : mb2DdlTable.getTableColumns()) {
			if (null != createTableParam.getFiledUniqueName()) {
				addUniqueFieldList.add(createTableParam);
			}
		}
		return addUniqueFieldList;
	}

}
