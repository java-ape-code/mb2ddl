package com.codecate.ccfun.mb2ddl.model;

import java.util.ArrayList;
import java.util.List;

import com.codecate.ccfun.mb2ddl.constants.Constants;

import lombok.Data;

@Data
public class Mb2DdlTable {
	
	
	private String tableName;
	private String tableComment;
	private Constants.MySqlCharset tableCharset;
	private Constants.MySqlEngine tableEngine;
	private List<Mb2DdlColumn> tableColumns = new ArrayList<>();
	
	//db
	private MysqlTable dbMysqlTable;
	private List<MysqlColumn> dbTableColumnList;
	private List<String> dbIndexNameList;
	
}
