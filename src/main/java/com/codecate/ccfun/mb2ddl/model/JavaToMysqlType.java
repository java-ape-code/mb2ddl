package com.codecate.ccfun.mb2ddl.model;


import java.util.HashMap;
import java.util.Map;

import com.codecate.ccfun.mb2ddl.constants.Constants;

public class JavaToMysqlType {
    public static Map<String, Constants.MySqlType> javaToMysqlTypeMap = new HashMap<String, Constants.MySqlType>();
    static {
        javaToMysqlTypeMap.put("class java.lang.String", Constants.MySqlType.VARCHAR);
        javaToMysqlTypeMap.put("class java.lang.Long", Constants.MySqlType.BIGINT);
        javaToMysqlTypeMap.put("class java.lang.Integer", Constants.MySqlType.INT);
        javaToMysqlTypeMap.put("class java.lang.Boolean", Constants.MySqlType.BIT);
        javaToMysqlTypeMap.put("class java.math.BigInteger", Constants.MySqlType.BIGINT);
        javaToMysqlTypeMap.put("class java.lang.Float", Constants.MySqlType.FLOAT);
        javaToMysqlTypeMap.put("class java.lang.Double", Constants.MySqlType.DOUBLE);
        javaToMysqlTypeMap.put("class java.lang.Short", Constants.MySqlType.SMALLINT);
        javaToMysqlTypeMap.put("class java.math.BigDecimal", Constants.MySqlType.DECIMAL);
        javaToMysqlTypeMap.put("class java.sql.Date", Constants.MySqlType.DATE);
        javaToMysqlTypeMap.put("class java.util.Date", Constants.MySqlType.DATE);
        javaToMysqlTypeMap.put("class java.sql.Timestamp", Constants.MySqlType.DATETIME);
        javaToMysqlTypeMap.put("class java.sql.Time", Constants.MySqlType.TIME);
        javaToMysqlTypeMap.put("class java.time.LocalDateTime", Constants.MySqlType.DATETIME);
        javaToMysqlTypeMap.put("class java.time.LocalDate", Constants.MySqlType.DATE);
        javaToMysqlTypeMap.put("class java.time.LocalTime", Constants.MySqlType.TIME);
        javaToMysqlTypeMap.put("long", Constants.MySqlType.BIGINT);
        javaToMysqlTypeMap.put("int", Constants.MySqlType.INT);
        javaToMysqlTypeMap.put("boolean", Constants.MySqlType.BIT);
        javaToMysqlTypeMap.put("float", Constants.MySqlType.FLOAT);
        javaToMysqlTypeMap.put("double", Constants.MySqlType.DOUBLE);
        javaToMysqlTypeMap.put("short", Constants.MySqlType.SMALLINT);
        javaToMysqlTypeMap.put("char", Constants.MySqlType.VARCHAR);
    }
}
