package com.codecate.ccfun.mb2ddl.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class TableConfig {

    private List<?> list = new ArrayList<>();

    private Map<String,Object> map = new HashMap<String, Object>();

    public TableConfig(List<?> list, Map<String, Object> map) {
        if (list != null){
            this.list = list;
        }
        if(map != null){
            this.map = map;
        }
    }

    public TableConfig(List<?> list) {
        if (list != null){
            this.list = list;
        }
    }

    public TableConfig(Map<String, Object> map) {
        this.map = map;
    }

}
