package com.codecate.ccfun.mb2ddl.model;

import lombok.Data;

@Data
public class MySqlTypeAndLength {
    private Integer lengthCount;
    private Integer length;
    private Integer decimalLength;
    private String  type;

    public MySqlTypeAndLength(){

    }

    public MySqlTypeAndLength(Integer lengthCount, Integer length, Integer decimalLength, String type){
        this.lengthCount = lengthCount;
        this.type = type;
        this.length = length;
        this.decimalLength = decimalLength;
    }

}
