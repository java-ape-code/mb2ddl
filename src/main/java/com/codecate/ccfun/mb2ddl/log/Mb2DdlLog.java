package com.codecate.ccfun.mb2ddl.log;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Mb2DdlLog {
	
	private static final Logger log = LoggerFactory.getLogger(Mb2DdlLog.class);
	private static String level = "error";
	
	public static void logMsg(String msg) {
		if (StringUtils.equals("error", Mb2DdlLog.level)) {
			Mb2DdlLog.log.error(msg);
		}else if (StringUtils.equals("info", Mb2DdlLog.level)) {
			Mb2DdlLog.log.info(msg);
		}else if (StringUtils.equals("debug", Mb2DdlLog.level)) {
			Mb2DdlLog.log.debug(msg);
		}else if (StringUtils.equals("warn", Mb2DdlLog.level)) {
			Mb2DdlLog.log.warn(msg);
		}
		
	}
	
	public static void logErr(String msg) {
		Mb2DdlLog.log.error(msg);
	}
	
	public static void logErr(Exception e) {
		String msg = "";
		if (null!=e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw, true));
	        msg = msg + ","+ "exception=" + sw.getBuffer().toString();
			try {
				sw.close();
			} catch (IOException tmp) {
				tmp.printStackTrace();
			}
		}
		Mb2DdlLog.log.error(msg);
	}

}
