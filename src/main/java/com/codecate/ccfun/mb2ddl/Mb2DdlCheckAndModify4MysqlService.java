package com.codecate.ccfun.mb2ddl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.baomidou.mybatisplus.annotation.TableName;
import com.codecate.ccfun.mb2ddl.annotation.Table;
import com.codecate.ccfun.mb2ddl.constants.Constants;
import com.codecate.ccfun.mb2ddl.dao.Mb2DdlCreateMysqlTablesMapper;
import com.codecate.ccfun.mb2ddl.log.Mb2DdlLog;
import com.codecate.ccfun.mb2ddl.model.Mb2DdlColumn;
import com.codecate.ccfun.mb2ddl.model.Mb2DdlTable;
import com.codecate.ccfun.mb2ddl.model.MysqlColumn;
import com.codecate.ccfun.mb2ddl.utils.ClassScaner;

@Service
public class Mb2DdlCheckAndModify4MysqlService {

	private static String entityPath = null;
	private static String active = Constants.Mb2DdlActive.NONE.toString();

	@Autowired
	Mb2DdlConfigurationUtil springContextUtil;
	@Autowired
	Mb2DdlClassParse4MysqlService classParse4MysqlService;
	@Autowired
	Mb2DdlCreateMysqlTablesMapper createMysqlTablesMapper;
	@Autowired
	Mb2DdlModify2DbService modif2DbService;
	@Autowired
	Mb2DdlCreateSql4MysqlService createSql4MysqlService;

	public void checkAndModify() {
		entityPath = springContextUtil.getConfig(Constants.MB2DDL_ENTITY_PATH_KEY);
		active = springContextUtil.getConfig(Constants.MB2DDL_ACTIVE_KEY);
		if (StringUtils.isBlank(active)
				|| Constants.Mb2DdlActive.NONE.equals(Constants.Mb2DdlActive.valueOf(active.toUpperCase()))
				|| StringUtils.isBlank(entityPath) || !Constants.Mb2DdlActive.contains(active.toUpperCase())) {
			return;
		}
		// 拆成多个pack，支持多个
		String[] packs = entityPath.split(",|;");
		Set<Class> classes = ClassScaner.scan(packs, Table.class, TableName.class, javax.persistence.Table.class);
		;
		List<Mb2DdlTable> tables = classParse4MysqlService.parseClazzToTables(classes);
		List<String> tableNames = createMysqlTablesMapper.tableNames();
		if (!CollectionUtils.isEmpty(tableNames)) {
			for (String name : tableNames) {
				if (!CollectionUtils.isEmpty(tables)) {
					boolean have = false;
					for (Mb2DdlTable mb2DdlTable : tables) {
						if (StringUtils.equals(name, mb2DdlTable.getTableName())) {
							have = true;
						}
					}
					if (!have) {
						Mb2DdlLog.logErr("需要删除的数据库表名=" + name);
					}
				}
			}
		}
		Mb2DdlLog.logErr("检查需要删除的数据库表结束");
		Collections.sort(tables, new Comparator<Mb2DdlTable>() {
			@Override
			public int compare(Mb2DdlTable o1, Mb2DdlTable o2) {
				return o1.getTableName().compareTo(o2.getTableName());
			}
		});

		if (!CollectionUtils.isEmpty(tables)) {
			// 创建表sql
			// 添加index
			// 添加unique
			Mb2DdlDiff2DBService diffService = new Mb2DdlDiff2DBService();
			for (Mb2DdlTable mb2DdlTable : tables) {
				Mb2DdlLog.logErr("处理数据库表开始==" + mb2DdlTable.getTableName());
				if (Constants.Mb2DdlActive.ONLYDDL.equals(Constants.Mb2DdlActive.valueOf(active.toUpperCase()))) {
					// create table sql
					List<Mb2DdlColumn> allKeys = diffService.getAllKeyFieldList(mb2DdlTable);
					createSql4MysqlService.makeCreateTableSql(mb2DdlTable,allKeys);
					// create index sql
					List<Mb2DdlColumn> allIndexs = diffService.getAllIndexList(mb2DdlTable);
					createSql4MysqlService.makeCreateIndexSql(mb2DdlTable,allIndexs);
					// create unique sql
					List<Mb2DdlColumn> allUniques = diffService.getAllUniqueList(mb2DdlTable);
					createSql4MysqlService.makeCreateUniqueSql(mb2DdlTable,allUniques);

					continue;
				}
				// need add remove modify cloumn index unique
				// 2. 找出增加的字段
				List<Mb2DdlColumn> addFieldList = diffService.getAddFieldList(mb2DdlTable);
				// 3. 找出删除的字段
				List<MysqlColumn> removeFieldList = diffService.getRemoveFieldList(mb2DdlTable);
				// 4. 找出更新的字段
				List<Mb2DdlColumn> modifyFieldList = diffService.getModifyFieldList(mb2DdlTable);
				// 5. 找出需要删除主键的字段
				List<Mb2DdlColumn> dropKeyFieldList = diffService.getDropKeyFieldList(mb2DdlTable);
				// 5. 找出需要更新成当前主键字段
				List<Mb2DdlColumn> currKeyFieldList = diffService.getCurrKeyFieldList(mb2DdlTable);
				// 6. 找出需要删除的索引和唯一约束
				List<String> dropIndexAndUniqueFieldList = diffService.getDropIndexAndUniqueList(mb2DdlTable);
				// 7. 找出需要新增的索引
				List<Mb2DdlColumn> addIndexFieldList = diffService.getAddIndexList(mb2DdlTable);
				// 8. 找出需要新增的唯一约束
				List<Mb2DdlColumn> addUniqueFieldList = diffService.getAddUniqueList(mb2DdlTable);

				if (Constants.Mb2DdlActive.CHECK.equals(Constants.Mb2DdlActive.valueOf(active.toUpperCase()))
						||Constants.Mb2DdlActive.ADD.equals(Constants.Mb2DdlActive.valueOf(active.toUpperCase()))
						||Constants.Mb2DdlActive.UPDATE.equals(Constants.Mb2DdlActive.valueOf(active.toUpperCase()))) {
					Mb2DdlLog.logErr("检查开始");
					// 2. 找出增加的字段
					if (!CollectionUtils.isEmpty(addFieldList)) {
						String logInfoMsg = "";
						for (Mb2DdlColumn tmp : addFieldList) {
							logInfoMsg = logInfoMsg + tmp.getFieldName() + ",";
						}
						Mb2DdlLog.logErr("检查2. 找出增加的字段==="+mb2DdlTable.getTableName()+"==="+logInfoMsg);
					}
					// 3. 找出删除的字段
					if (!CollectionUtils.isEmpty(removeFieldList)) {
						String logInfoMsg = "";
						for (MysqlColumn tmp : removeFieldList) {
							logInfoMsg = logInfoMsg + tmp.getColumn_name() + ",";
						}
						Mb2DdlLog.logErr("检查3. 找出删除的字段==="+mb2DdlTable.getTableName()+"==="+logInfoMsg);
					}
					// 4. 找出更新的字段
					if (!CollectionUtils.isEmpty(modifyFieldList)) {
						String logInfoMsg = "";
						for (Mb2DdlColumn tmp : modifyFieldList) {
							logInfoMsg = logInfoMsg + tmp.getFieldName() + ",";
						}
						Mb2DdlLog.logErr("检查4. 找出更新的字段==="+mb2DdlTable.getTableName()+"==="+logInfoMsg);
					}
					// 5. 找出需要删除主键的字段
					if (!CollectionUtils.isEmpty(dropKeyFieldList)) {
						String logInfoMsg = "";
						for (Mb2DdlColumn tmp : dropKeyFieldList) {
							logInfoMsg = logInfoMsg + tmp.getFieldName() + ",";
						}
						Mb2DdlLog.logErr("检查5. 找出需要删除主键的字段==="+mb2DdlTable.getTableName()+"==="+logInfoMsg);
					}
					// 5. 找出需要更新成当前主键字段
					if (!CollectionUtils.isEmpty(currKeyFieldList)) {
						String logInfoMsg = "";
						for (Mb2DdlColumn tmp : currKeyFieldList) {
							logInfoMsg = logInfoMsg + tmp.getFieldName() + ",";
						}
						Mb2DdlLog.logErr("检查5.1 找出需要更新成当前主键字段==="+mb2DdlTable.getTableName()+"==="+logInfoMsg);
					}
					// 6. 找出需要删除的索引和唯一约束
					if (!CollectionUtils.isEmpty(dropIndexAndUniqueFieldList)) {
						String logInfoMsg = "";
						for (String tmp : dropIndexAndUniqueFieldList) {
							logInfoMsg = logInfoMsg + tmp + ",";
						}
						Mb2DdlLog.logErr("检查6. 找出需要删除的索引和唯一约束==="+mb2DdlTable.getTableName()+"==="+logInfoMsg);
					}
					// 7. 找出需要新增的索引
					if (!CollectionUtils.isEmpty(addIndexFieldList)) {
						String logInfoMsg = "";
						for (Mb2DdlColumn tmp : addIndexFieldList) {
							logInfoMsg = logInfoMsg + tmp.getFieldName() + ",";
						}
						Mb2DdlLog.logErr("检查7. 找出需要新增的索引==="+mb2DdlTable.getTableName()+"==="+logInfoMsg);
					}
					// 8. 找出需要新增的唯一约束
					if (!CollectionUtils.isEmpty(addUniqueFieldList)) {
						String logInfoMsg = "";
						for (Mb2DdlColumn tmp : addUniqueFieldList) {
							logInfoMsg = logInfoMsg + tmp.getFieldName() + ",";
						}
						Mb2DdlLog.logErr("检查8. 找出需要新增的唯一约束==="+mb2DdlTable.getTableName()+"==="+logInfoMsg);
					}

					Mb2DdlLog.logErr("检查结束");
				}
				if (Constants.Mb2DdlActive.ADD.equals(Constants.Mb2DdlActive.valueOf(active.toUpperCase()))
						|| Constants.Mb2DdlActive.UPDATE.equals(Constants.Mb2DdlActive.valueOf(active.toUpperCase()))) {
					if (null == mb2DdlTable.getDbMysqlTable()) {
						// 1. 创建表
						this.modif2DbService.createTableByMap(mb2DdlTable, currKeyFieldList);
					} else {
						if (Constants.Mb2DdlActive.UPDATE
								.equals(Constants.Mb2DdlActive.valueOf(active.toUpperCase()))) {
							if (CollectionUtils.isEmpty(currKeyFieldList)) {
								// 2. 删除要变更主键的表的原来的字段的主键
								this.modif2DbService.dropFieldsKeyByMap(mb2DdlTable, dropKeyFieldList);
							}
							// 3. 删除索引和约束
							this.modif2DbService.dropIndexAndUniqueByMap(mb2DdlTable, dropIndexAndUniqueFieldList);
							// 4. 删除字段
							this.modif2DbService.removeFieldsByMap(mb2DdlTable, removeFieldList);
							// 5. 修改表注释
							this.modif2DbService.modifyTableCommentByMap(mb2DdlTable);
							// 6. 修改字段类型等
							this.modif2DbService.modifyFieldsByMap(mb2DdlTable, modifyFieldList);
						}
						// 7. 添加新的字段
						this.modif2DbService.addFieldsByMap(mb2DdlTable, addFieldList);
					}
					// 8. 创建索引
					this.modif2DbService.addIndexByMap(mb2DdlTable, addIndexFieldList);
					// 9. 创建约束
					this.modif2DbService.addUniqueByMap(mb2DdlTable, addUniqueFieldList);
					// 10.修改主键
					this.modif2DbService.changeFieldsKeyByMap(mb2DdlTable, currKeyFieldList);

				}
				Mb2DdlLog.logErr("处理数据库表结束==" + mb2DdlTable.getTableName());
			}
		}
	}

}
