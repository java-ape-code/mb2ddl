package com.codecate.ccfun.mb2ddl;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codecate.ccfun.mb2ddl.log.Mb2DdlLog;

@Service
public class Mb2DdlStartImpl implements Mb2DdlStart{
	
	@Autowired
	Mb2DdlCheckAndModify4MysqlService checkAndModifyService;

	@PostConstruct
	public void startHandle() {
		System.out.println(" _ _   |_  _ _|_. ___              ");
		System.out.println("| | |\\/|_)(_| | |_\\  2DDL   0.0.1");
		System.out.println("     /                             ");
		System.out.println("                                   ");
		Mb2DdlLog.logErr("mybatis2ddl启动");
		this.checkAndModifyService.checkAndModify();
		Mb2DdlLog.logErr("mybatis2ddl结束");
	}

}
