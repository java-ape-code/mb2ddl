package com.codecate.ccfun.mb2ddl.constants;

import java.util.HashMap;
import java.util.Map;

import com.codecate.ccfun.mb2ddl.model.MySqlTypeAndLength;

public class Constants {
	
	public static final String MB2DDL_ENTITY_PATH_KEY 			= "mb2ddl.entity.path";
	public static final String MB2DDL_ACTIVE_KEY 				= "mb2ddl.active";
	
	public static final String MB2DDL_ENTITY_PATH_KEY_VALUE 	= "${mb2ddl.entity.path:NULL}";
	public static final String MB2DDL_ACTIVE_KEY_VALUE 			= "${mb2ddl.active:NULL}";

	public static final String NULL 							= "NULL";

	public static final String NEW_TABLE_MAP					= "newTableMap";
	public static final String MODIFY_TABLE_MAP					= "modifyTableMap";
	public static final String ADD_TABLE_MAP					= "addTableMap";
	public static final String REMOVE_TABLE_MAP					= "removeTableMap";
	public static final String MODIFY_TABLE_PROPERTY_MAP 		= "modifyTablePropertyMap";
	public static final String DROPKEY_TABLE_MAP				= "dropKeyTableMap";
	public static final String DROPINDEXANDUNIQUE_TABLE_MAP		= "dropIndexAndUniqueTableMap";
	public static final String ADDINDEX_TABLE_MAP				= "addIndexTableMap";
	public static final String ADDUNIQUE_TABLE_MAP				= "addUniqueTableMap";

	public static final String TABLE_INDEX_KEY 					= "mb2ddl.index.prefix";
	public static final String TABLE_UNIQUE_KEY 				= "mb2ddl.unique.prefix";

	public static final String TABLE_INDEX_KEY_VALUE 			= "${mb2ddl.index.prefix:mb2ddl_idx_}";
	public static final String TABLE_UNIQUE_KEY_VALUE 			= "${mb2ddl.unique.prefix:mb2ddl_uni_}";
	
	
	public enum Mb2DdlActive {
		NONE,   //不操作
		CHECK,  //检查不操作
		ADD,    //添加表
		UPDATE, //更新表
		ONLYDDL;//输出sql
		
		public static boolean contains(String str) { 
			for (Mb2DdlActive active : Mb2DdlActive.values()) {
				if (active.name().equals(str)) { 
					return true; 
				} 
			} 
			return false; 
		}
	}
	
	public enum MySqlCharset {
	    DEFAULT,
	    ARMSCII8,
	    ASCII,
	    BIG5,
	    BINARY,
	    CP850,
	    CP852,
	    CP866,
	    CP932,
	    CP1250,
	    CP1251,
	    CP1256,
	    CP1257,
	    DEC8,
	    EUCJPMS,
	    EUCKR,
	    GB2312,
	    GBK,
	    GEOSTD8,
	    GREEK,
	    HEBREW,
	    HP8,
	    KEYBCS2,
	    KOI8R,
	    KOI8U,
	    LATIN1,
	    LATIN2,
	    LATIN5,
	    LATIN7,
	    MACCE,
	    MACROMAN,
	    SJIS,
	    SWE7,
	    TIS620,
	    UCS2,
	    UJIS,
	    UTF8,
	    UTF8MB4,
	    UTF16,
	    UTF32;
	}
	
	public enum  MySqlEngine {

	    DEFAULT,
	    ARCHIVE,
	    BLACKHOLE,
	    CSV,
	    InnoDB,
	    MEMORY,
	    MRG_MYISAM,
	    MyISAM,
	    PERFORMANCE_SCHEMA;
	}
	
	
	public enum MySqlType {

		DEFAULT(null,null,null),
		INT(1, 11, null),
		VARCHAR(1, 255, null),
		BINARY(1, 1, null),
		CHAR(1, 255, null),
		BIGINT(1, 20, null),
		BIT(1, 1, null),
		TINYINT(1, 4, null),
		SMALLINT(1, 6, null),
		MEDIUMINT(1, 9, null),
		DECIMAL(2, 10, 2),
		DOUBLE(2, 10, 0),
		TEXT(0, null, null),
		MEDIUMTEXT(0, null, null),
		LONGTEXT(0, null, null),
		DATETIME(0, null, null),
		TIMESTAMP(0, null, null),
		DATE(0, null, null),
		TIME(0, null, null),
		FLOAT(2, 10, 0),
		YEAR(0, null, null),
		BLOB(0, null, null),
		LONGBLOB(0, null, null),
		MEDIUMBLOB(0, null, null),
		TINYTEXT(0, null, null),
		TINYBLOB(0, null, null),
		JSON(0, null, null);

		private Integer lengthCount;
		private Integer lengthDefault;
		private Integer decimalLengthDefault;

		public Integer getLengthCount() {
			return lengthCount;
		}

		public Integer getLengthDefault() {
			return lengthDefault;
		}

		public Integer getDecimalLengthDefault() {
			return decimalLengthDefault;
		}

		MySqlType(Integer lengthCount, Integer lengthDefault, Integer decimalLengthDefault){
			this.lengthCount = lengthCount;
			this.lengthDefault = lengthDefault;
			this.decimalLengthDefault = decimalLengthDefault;
		}

		// 获取Mysql的类型，以及类型需要设置几个长度，如需扩展改变这个对象的值即可
		/**
		 * 获取Mysql的类型，以及类型需要设置几个长度，这里构建成map的样式
		 * 构建Map(字段名(小写),需要设置几个长度(0表示不需要设置，1表示需要设置一个，2表示需要设置两个))
		 */
		public static Map<String, MySqlTypeAndLength> mySqlTypeAndLengthMap;

		static {
			mySqlTypeAndLengthMap = new HashMap<String, MySqlTypeAndLength>();
			for (MySqlType type: MySqlType.values()) {
				mySqlTypeAndLengthMap.put(type.toString().toLowerCase(), new MySqlTypeAndLength(type.getLengthCount(), type.getLengthDefault(), type.getDecimalLengthDefault(), type.toString().toLowerCase()));
			}
		}
	}

}
